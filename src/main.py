# settings
WAIT_TIME = 1
IP_FILE = "ip"
SHOW_DEBUG = False
SHOW_INFO = True
SERVER_NAME = "PI-DEMO"

# login info
gmail_user = 'redacted@gmail.com'
gmail_password = 'redacted'

# attempt to import modules
import subprocess
try:
    if SHOW_DEBUG: print("[INIT:DEBUG] Attempting to import module \"smtplib\"...")
    import smtplib
    if SHOW_INFO: print("[INIT:INFO] Module \"smtplib\" imported correctly.")
except:
    print("[INIT:ERROR] Module \"smtplib\" could not be imported.")
    print("[INIT:ERROR] Please check docs/dependencies for required modules.")
    exit()
try:
    if SHOW_DEBUG: print("[INIT:DEBUG] Attempting to import module \"requests\"...")
    from requests import get
    if SHOW_INFO: print("[INIT:INFO] Module \"requests\" imported correctly.")
except:
    print("[INIT:ERROR] Module \"requests\" could not be imported.")
    print("[INIT:ERROR] Please check docs/dependencies for required modules.")
    exit()
try:
    if SHOW_DEBUG: print("[INIT:DEBUG] Attempting to import module \"os\"...")
    import time
    if SHOW_INFO: print("[INIT:INFO] Module \"time\" imported correctly.")
except:
    print("[INIT:ERROR] Module \"time\" could not be imported.")
    print("[INIT:ERROR] Please check docs/dependencies for required modules.")
    exit()

# send the email
def sendEmail(toSend,OVERRIDE_TEMPLATE=False):
    sent_from = gmail_user
    to = ['redacted@redacted.com']
    if OVERRIDE_TEMPLATE:
        email_text = toSend
    else:
        email_text = ("The new SSH address for %s is\n%s\n" % (SERVER_NAME,toSend))
    try:
        if SHOW_DEBUG: print("[EMAIL:DEBUG] Attempting to login to gmail...")
        server = smtplib.SMTP_SSL('smtp.gmail.com', 465)
        server.ehlo()
        server.login(gmail_user, gmail_password)
        if SHOW_DEBUG: print("[EMAIL:DEBUG] Logged into gmail.")
        server.sendmail(sent_from, to, email_text)
        if SHOW_INFO: print("[EMAIL:INFO] Sent email.")
        server.close()
        if SHOW_DEBUG: print("[EMAIL:DEBUG] Closed connection to server.")
    except:  
        print('[EMAIL:ERROR] Something failed to run.')

# get the external ip address
def getIPAddress():
    while True:
        try:
            ip = subprocess.Popen("ip address show wlan0", shell=True, stdout=subprocess.PIPE).stdout.read().decode().split()[18]
            break
        except:
            if SHOW_DEBUG: print("[IPAPI:DEBUG] The IP website could not be reached. Trying again in 30 seconds.")
            wait(0.5)
    if SHOW_DEBUG: print("[IPAPI:DEBUG] The IP api returned address \"%s\"" % (ip))
    return ip

# wait for a certain amount of time
def wait(sleepTime=WAIT_TIME):
    time.sleep(sleepTime * 60)

# open stored ip address file and write to it
def writeIP(ipAddress):
    try:
        if SHOW_DEBUG: print("[WRITE:DEBUG] Open file \"%s\" for writing..." % (IP_FILE))
        f = open(IP_FILE,"w")
        if SHOW_DEBUG: print("[WRITE:DEBUG] Writing \"%s\" to file..." % (ipAddress))
        f.write(ipAddress)
        if SHOW_DEBUG: print("[WRITE:DEBUG] Closing file...")
        f.close()
        if SHOW_INFO: print("[WRITE:INFO] File written to disk.")
    except:
        print("[WRITE:ERROR] Failed to write to file \"%s\"." % (IP_FILE))

# open stored IP Address and read it
def readIP():
    try:
        if SHOW_DEBUG: print("[READ:DEBUG] Open file \"%s\" for reading..." % (IP_FILE))
        f = open(IP_FILE,"r")
        if SHOW_DEBUG: print("[READ:DEBUG] Reading file...")
        ip = f.readline()
        if SHOW_DEBUG: print("[READ:DEBUG] Closing file...")
        f.close()
        if SHOW_INFO: print("[READ:INFO] Read \"%s\" from disk." % (ip))
        return ip
    except:
        print("[READ:ERROR] Failed to read file \"%s\"." % (IP_FILE))

# send email and update IP file
def update(ipAddr):
    if SHOW_DEBUG: print("[UPDATE:DEBUG] Sending IP Address...")
    sendEmail(ipAddr)
    if SHOW_INFO: print("[UPDATE:INFO] IP Address sent to email.")
    if SHOW_DEBUG: print("[UPDATE:DEBUG] Writing IP Address to file...")
    writeIP(ipAddr)
    if SHOW_INFO: print("[UPDATE:INFO] Wrote IP to file.")

# initialize program
def init():
    # check if program has ran before
    if readIP() == None:
        print("[INIT:WARN] Could not load IP address from file. Assuming this is the first run.")
        if SHOW_DEBUG: print("[INIT:DEBUG] Initialize program by getting IP address and running update function...")
        update(getIPAddress())
        if SHOW_INFO: print("[INIT:INFO] Program initialized.")
    if SHOW_DEBUG: print("[INIT:DEBUG] Entering main loop...")
    # catch any errors
    try:
        # enter main loop
        main()
    except KeyboardInterrupt:
        # abort due to keyboard interrupt
        print("\n[PYTHON:WARN] Program will exit due to keyboard interrupt.")
        exit()
    except BaseException as e:
        # write traceback to console and exit
        print("[PYTHON:ERROR] An uncaught error occured.")
        print("[PYTHON:ERRPR] === Printing traceback ===")
        print(str(e))
        if SHOW_INFO: print("[PYTHON:INFO] Program will now exit.")
        exit()

# tie everything together
def main():
    # enter main loop
    while True:
        # get IP Address
        ip = getIPAddress()
        # check if IP address changed
        if ip != readIP():
            print("[MAINL:INFO] IP Address changed.")
            if SHOW_DEBUG: print("[MAINL:DEBUG] Running update function...")
            update(ip)
        if SHOW_DEBUG: print("[MAINL:DEBUG] Waiting for %s minutes." % (WAIT_TIME))
        wait()

if __name__ == "__main__":
    # start program
    init()
